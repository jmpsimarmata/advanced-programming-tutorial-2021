package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "Evo Shield Here Level 5!";
    }

    @Override
    public String getType() {
        return "Armor";
    }
    //ToDo: Complete me
}
