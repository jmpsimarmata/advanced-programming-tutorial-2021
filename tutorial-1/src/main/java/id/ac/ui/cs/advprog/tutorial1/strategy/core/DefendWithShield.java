package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "I have my Dome for Protection -Gibraltar";
    }

    @Override
    public String getType() {
        return "Shield";
    }
    //ToDo: Complete me
}
