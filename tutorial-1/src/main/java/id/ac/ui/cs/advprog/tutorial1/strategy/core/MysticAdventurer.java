package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(){
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }
    @Override
    public String getAlias() {
        return "Mystic Magic is so wonderful -Gord";
    }
    //ToDo: Complete me
}
