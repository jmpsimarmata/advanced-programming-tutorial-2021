package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> chainSpellList;
    public ChainSpell(ArrayList<Spell> chainSpellList){
        this.chainSpellList = chainSpellList;
    }

    @Override
    public void cast() {
        for(Spell spell: chainSpellList){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = chainSpellList.size()-1; i >= 0; i--){
            chainSpellList.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
