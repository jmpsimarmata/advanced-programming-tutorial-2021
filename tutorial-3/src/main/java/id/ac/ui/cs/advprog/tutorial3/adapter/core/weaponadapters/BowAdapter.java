package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean aimshot;

    public BowAdapter(Bow bow){
        this.bow = bow;
        this.aimshot = false;
    }

    public boolean getAimShot(){
        return this.aimshot;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(aimshot);
    }

    @Override
    public String chargedAttack() {
        if(this.aimshot){
            this.aimshot = false;
            return "Aim shot mode turned off";
        }else {
            this.aimshot = true;
            return "Aim Shot Turned on";
        }
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
