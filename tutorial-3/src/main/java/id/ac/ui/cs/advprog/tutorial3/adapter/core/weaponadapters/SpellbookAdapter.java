package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean largeSpellCasted;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.largeSpellCasted = false;
    }

    public boolean getLargeSpellCasted(){
        return this.largeSpellCasted;
    }

    @Override
    public String normalAttack() {
        this.largeSpellCasted = false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(this.largeSpellCasted){
            return "Magic power not enough for large spell";
        }
        this.largeSpellCasted = true;
        return this.spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
