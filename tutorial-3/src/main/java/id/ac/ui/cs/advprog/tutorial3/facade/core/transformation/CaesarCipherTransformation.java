package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarCipherTransformation extends Transformation {
    private int key;

    public CaesarCipherTransformation(){this.key = 5;}
    public CaesarCipherTransformation(int key){this.key = key;}

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        int codexSize = codex.getCharSize();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            char oldCharacter = text.charAt(i);
            int charIdx = codex.getIndex(oldCharacter);
            int newCharIdx = charIdx + selector * this.key;
            newCharIdx = newCharIdx < 0? codexSize +  newCharIdx : newCharIdx % codexSize;
            char newCharacter = codex.getChar(newCharIdx);
            res[i] = newCharacter;

        }
        return new Spell(new String(res), codex);
    }

}
