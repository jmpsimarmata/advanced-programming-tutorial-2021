package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class Solver {
    private final List<Transformation> transformationList;

    public Solver(){
        this.transformationList = new ArrayList<Transformation>();
        this.transformationList.add(new CelestialTransformation());
        this.transformationList.add(new AbyssalTransformation());
        this.transformationList.add(new CaesarCipherTransformation());
    }

    public Solver(String customCelestial, int customAbyssal, int customCaesarCipher){
        this.transformationList = new ArrayList<Transformation>();
        this.transformationList.add(new CelestialTransformation(customCelestial));
        this.transformationList.add(new AbyssalTransformation(customAbyssal));
        this.transformationList.add(new CaesarCipherTransformation(customCaesarCipher));
    }

    public String encode(String text){
        Spell newSpell = new Spell(text, AlphaCodex.getInstance());
        for(Transformation transformation: transformationList){
            newSpell = transformation.encode(newSpell);
        }
        Spell spellResult = CodexTranslator.translate(newSpell, RunicCodex.getInstance());
        return spellResult.getText();
    }

    public String decode(String text){
        Spell newSpell = new Spell(text, RunicCodex.getInstance());
        newSpell = CodexTranslator.translate(newSpell, AlphaCodex.getInstance());
        for(int i = transformationList.size()-1; i>=0; i--){
            newSpell = transformationList.get(i).decode(newSpell);
        }
        return newSpell.getText();

    }
}
