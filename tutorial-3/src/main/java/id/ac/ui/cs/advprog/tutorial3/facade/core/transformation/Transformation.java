package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public abstract class Transformation {

    public abstract Spell encode(Spell spell);
    public abstract Spell decode(Spell spell);

}
