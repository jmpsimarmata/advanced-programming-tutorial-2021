package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SolverTest {
    private Class<?> solverClass;
    private Solver solver, solverWithCustomKey;

    @BeforeEach
    public void setup() throws Exception{
        solverClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Solver");
        solver = new Solver();
        solverWithCustomKey = new Solver("iamtheboneofmysword", 5,5);
        //Customkey nya sama seperti defaultnya
    }

    @Test
    public void testSolverHasEncodeMethod() throws Exception{
        Method encode = solverClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, encode.getParameterCount());
        assertEquals("java.lang.String", encode.getGenericReturnType().getTypeName());
    }

    @Test
    public void testSolverHasDecodeMethod() throws Exception{
        Method decode = solverClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, decode.getParameterCount());
        assertEquals("java.lang.String", decode.getGenericReturnType().getTypeName());
    }

    @Test
    public void testEncodeMethodHasACorrectResult(){
        String result = solver.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals("!|G*^ccD^#m:M%deeq-!]}y!!|BiZAvS@!%N__u<&yZ!DF@?F)_w", result);
    }

    @Test
    public void testEncodeMethodWithCustomKeyHasACorrectResult(){
        String result = solverWithCustomKey.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals("!|G*^ccD^#m:M%deeq-!]}y!!|BiZAvS@!%N__u<&yZ!DF@?F)_w", result);
    }

    @Test
    public void testDecodeMethodHasACorrectResult(){
        String result = solver.decode("!|G*^ccD^#m:M%deeq-!]}y!!|BiZAvS@!%N__u<&yZ!DF@?F)_w");
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }

    @Test
    public void testDecodeMethodWithCustomKeyHasACorrectResult(){
        String result = solverWithCustomKey.decode("!|G*^ccD^#m:M%deeq-!]}y!!|BiZAvS@!%N__u<&yZ!DF@?F)_w");
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }



}
