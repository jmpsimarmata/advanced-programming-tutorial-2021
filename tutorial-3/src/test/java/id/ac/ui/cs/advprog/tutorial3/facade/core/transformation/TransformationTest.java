package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransformationTest {
    private Class<?> transformationClass;

    @BeforeEach
    public void setup() throws Exception{
        transformationClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation");
    }

    @Test
    public void testTransformationIsAPublicAbstractClass(){
        int classModifiers = transformationClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

}
